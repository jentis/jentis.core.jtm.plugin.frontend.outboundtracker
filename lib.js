function(jtsTracker,oGateConfig){

    this.oConf = oGateConfig;
	this.oTracker = jtsTracker;

	this.init = function(){
		this.oTracker.registerGateFunc("pageview",this.pageview,this);
	}
	
	this.pageview = function(){

        // Initiate the search when pageview happens
        this.searchForLinks();

	}
    
    this.fListen =  function(oObject, sEvent, fFunction)
    {
        if (oObject.addEventListener)
        {
            oObject.addEventListener(sEvent, fFunction, false);
        }
        else
            if (oObject.attachEvent)
            {
                oObject.attachEvent("on" + sEvent, fFunction);
            }
    }

    this.searchForLinks = function ()
	{
		if (document.getElementsByTagName) 
		{
			var aLinks = document.getElementsByTagName('a');

			for (var i = 0, iMax = aLinks.length; i < iMax; i++) 
			{
                if (typeof aLinks[i].hostname !== "undefined" && aLinks[i].hostname.length > 0 && aLinks[i].hostname != location.hostname)
                {
                    this.addClickEventForOutgoingLinkTracking(aLinks[i]);
                }
                else
                {
                    // We have to also check other attributes f.e. data-href (f.e. Pinterest)
                    for (var x = 0; x < aLinks[i].attributes.length; x++) {
                        var attrib = aLinks[i].attributes[x];
                        if (attrib.specified) {
                            if(attrib.name.toLowerCase().indexOf("href") !== -1) {
                                if(attrib.value.startsWith("http"))
                                {
                                    var regex = new RegExp("https?:\/\/"+location.hostname,"i");
                                    if(!attrib.value.match(regex))
                                    {
                                        this.addClickEventForOutgoingLinkTracking(aLinks[i]);
                                    }
                                }
                            }
                        }
                    }
                }
			}
		}	
    }
    

    this.addClickEventForOutgoingLinkTracking = function(oElement) {

        this.fListen(oElement, "click", this.trackOutBoundLinks);

    }.bind(this)
    

    this.trackOutBoundLinks = function(oEvent) 
	{
		var oElement = this.fGetElementFromEvent(oEvent);
		
		if (/^(http|https|:)/i.test(oElement.protocol))
		{
			var sText = "_notext_";

			if(typeof oElement.href === "undefined" || !oElement.href.length)
			{
                for (var x = 0; x < oElement.attributes.length; x++) {
                    var attrib = oElement.attributes[x];
                    if (attrib.specified) {
                        if (attrib.name.toLowerCase().indexOf("href") !== -1) {
							oElement.href = attrib.value;
                        }
                    }
                }
			}

			/*
			 Finding texts of these elements:
			 1) img-tag ALT attribute
			 2) a-tag Text inside
			 3) _notext_
			 */

            // If <a> wraps an image, we try to get title of the image
            var aImgTags = oElement.getElementsByTagName("img");
            if(aImgTags.length > 0) {
                for(var i = 0; i < aImgTags.length; i++) {
                    if(aImgTags[i].alt !== "") {
                        sText = aImgTags[i].alt.trim();
                    }
                }
            }

            // Get content of the <a> tag
            if(oElement.text.trim() !== "") {
                sText = oElement.text.trim();
            }

            window._jts.push({
                track: "outlink",
                text: sText,
                url: oElement.href
            });
		}
    }.bind(this);
    
    // We want to support IE
    // If <a> tag has nested element, we want the parent(<a>) tag, not element itself
    this.fGetElementFromEvent = function(event) {
        let element = event.srcElement || event.target;

        if (element.tagName !== "A") {
            element = element.parentNode;
        }

        return element;
    }

	this.init();
}